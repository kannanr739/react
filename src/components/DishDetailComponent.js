import React,{Component} from 'react';
import { Card, CardBody, CardImg, CardText, CardTitle,Breadcrumb,BreadcrumbItem } from 'reactstrap';
import {Link }from'react-router-dom';
import { Modal, ModalHeader, ModalBody, Button,  Input, Label,Row,Col} from 'reactstrap';
import { Control, LocalForm,Errors} from 'react-redux-form';
import { Loading } from './LoadingComponent';
import{baseURL}from '../shared/baseUrl';
import {FadeTransform,Fade,Stagger} from 'react-animation-components';




   function RenderComments({comments,postComment,dishId}) {
        return (
            <>
            <ul className="list-unstyled ">
<Stagger in>
                {comments.map((comment) => {

                    return (
                        <Fade in>
                        <li key={comment.id}>
                            <p>{comment.comment}</p>
                            <p>-- {comment.author} ,{new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}  </p>
                        </li>
                        </Fade>
                    );
                })}
                </Stagger>
            </ul>
             <CommentForm dishId={dishId} postComment={postComment}/>
             </>
        );
    }

   function RenderDish({dish}) {

        return (
            <FadeTransform in transformProps={{exitTransform:'scale(0.5) translateY(-50%)' }}>
            <Card>
                <CardImg width="100%" src={baseURL+dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
            </FadeTransform>
        );
    }
    const DishDetail=(props)=> {
       
            
        if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">            
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">            
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if (props.dish != null) {
        return (
            <div className="container">
            <div className="row">
                <Breadcrumb>

                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.dish.name}</h3>
                    <hr />
                </div>                
            </div>
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={props.dish} />
                </div>
                <div className="col-12 col-md-5 m-1">
                    <h4>Comments</h4>
                    <RenderComments comments={props.comments}
                    postComment={props.postComment} 
                    dishId={props.dish.id}/>
                </div>
            </div>
           
            </div>
        
        );
        }
        else{
            return(
                <div/>
            )
        }
        


    }
    class CommentForm extends Component {
        constructor(props) {
            super(props);
            this.state = {
                selectedDish: null,

                isModalOpen: false
            };
            this.toggleModal = this.toggleModal.bind(this);
            this.handleSubmit=this.handleSubmit.bind(this);
        }
        
        toggleModal() {
            this.setState({
                isModalOpen: !this.state.isModalOpen
            });
        }
        handleSubmit(values) {
            alert("Username: " + this.rating.value + " Password: " 
            + " Remember: "+values.name );
            this.toggleModal();
            this.props.postComment(this.props.dishId, this.rating.value, values.name, this.comment.value);
       }
        render() {
            const required = (val) => val && val.length;
    const maxLength = (len) => (val) => !(val) || (val.length <= len);
    const minLength = (len) => (val) => val && (val.length >= len);
            return (
                <>
                    <Button outline onClick={this.toggleModal}>
                        <span className="fa fa-pencil fa-lg"></span> Submit Comment
                </Button>
                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>
                            Submit Comment
                </ModalHeader>
                        <ModalBody>
                            <LocalForm onSubmit={this.handleSubmit}>
                                <Row className="form-group">
                                    <Col>
                                    <Label htmlFor="rating">Rating</Label>
                                    <Input type="select" id="rating" name="rating" 
                                        innerRef={(input) => this.rating = input}>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </Input>
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Col md={12}>
                                        Your Name
    
                                    </Col>
                                    <Col md={12}>    
                                    <Control.text model=".name" id="name" name="name"
                                            placeholder="Your Name"
                                            className="form-control"
                                            innerRef={(input) => this.name = input}
                                            validators={{
                                                required,minLength:minLength(3),maxLength:maxLength(15)
                                            }}
                                             />
                                             <Errors
                                            className="text-danger"
                                            model=".name"
                                            show="touched"
                                            messages={{
                                                required: 'Required ',
                                                minLength: 'Must be greater than 2 characters',
                                                maxLength: 'Must be 15 characters or less'
                                            }}
                                         />
                                    </Col>  
                                </Row>
                                <Row className="form-group">
                                    <Col>
                                    <Label htmlFor="comment">Comments </Label>
                                    <Input type="textarea"  name="comment" rows="6"
                                        innerRef={(input) => this.comment = input} />
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                  <Col>
                                    <Button type="submit" value="submit" className="bg-primary">Submit</Button>
                                    </Col>
                                </Row>
                            </LocalForm>
                        </ModalBody>
                    </Modal>
                </>
            )
        }
    
    }

export default DishDetail;



